package dev.haddox.atlasgrah.generator;

import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class SeedGeneratorTest {
    
    @Test
    void generateFromNull() {
        assertEquals(- 1, SeedGenerator.generate(null, 0));
    }
    
    @Test
    void generateFromTestSystemNameAtZero() {
        assertEquals(452, SeedGenerator.generate("test", 0));
    }
    
    @Test
    void generateFromTestSystemNameAtOneThousand() {
        assertEquals(1452, SeedGenerator.generate("test", 1000));
    }
    
    @Test
    void generateFromSystemNameAtCurrentTime() {
        try {
            assertTrue(
                    SeedGenerator.generate(InetAddress.getLocalHost().getHostName(), System.currentTimeMillis()) > 1675313304
            );
        } catch(UnknownHostException e) {
            fail();
        }
    }
}
