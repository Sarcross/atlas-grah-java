package dev.haddox.atlasgrah.generator;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UuidGeneratorTest {
    @Test
    void fromString() {
        String testValue = "test_value";
        UUID expectedUuid = UUID.nameUUIDFromBytes(testValue.getBytes(StandardCharsets.UTF_8));
        UUID generatedUuid = UuidGenerator.fromString(testValue);
        assertEquals(expectedUuid, generatedUuid);
    }
}
