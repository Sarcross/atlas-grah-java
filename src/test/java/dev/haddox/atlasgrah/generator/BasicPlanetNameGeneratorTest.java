package dev.haddox.atlasgrah.generator;

import dev.haddox.atlasgrah.dao.NameComponentDao;
import dev.haddox.atlasgrah.data.PlanetName;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class BasicPlanetNameGeneratorTest {
    @Autowired
    NameComponentDao dao;
    
    @Test
    void generateTest() {
        BasicPlanetNameGenerator generator = new BasicPlanetNameGenerator();
        generator.setSeed(SeedGenerator.generate("test-system", 5520));
        generator.setDao(dao);
        generator.populateParts();
        PlanetName name;
        int iterations = 100_000;
        
        for(int runs = 0; runs < iterations; runs++) {
            name = generator.generate();
            assertNotNull(name);
        }
        log.info("Generated {} names for {} runs with {} calls to random.", generator.getHistory().size(), iterations,generator.getGenerationCount());
    }
}
