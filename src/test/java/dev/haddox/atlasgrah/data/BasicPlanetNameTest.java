package dev.haddox.atlasgrah.data;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
class BasicPlanetNameTest {
    
    @Test
    void constructor() {
        PlanetName planetName = new PlanetName();
        assertNull(planetName.getUuid());
        assertNotNull(planetName.getParts());
    }
    
    @Test
    void toStringTest() {
        NameComponent hello = new NameComponent("hello");
        NameComponent world = new NameComponent("world");
    
        PlanetName planetName = new PlanetName();
        planetName.addPart(hello);
        log.info(planetName.toString());
        assertEquals(hello.getValue(), planetName.toString());
    
        planetName.addPart(world);
        log.info(planetName.toString());
        assertEquals("helloworld", planetName.toString());
        assertEquals(UUID.nameUUIDFromBytes("helloworld".getBytes(StandardCharsets.UTF_8)), planetName.getUuid());
    }
}
