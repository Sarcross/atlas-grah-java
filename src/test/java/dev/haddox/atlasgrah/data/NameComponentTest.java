package dev.haddox.atlasgrah.data;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Slf4j
class NameComponentTest {
    
    @Test
    void constructorTest() {
        NameComponent component = new NameComponent();
        log.info(component.toString());
        assertEquals(UUID.nameUUIDFromBytes("".getBytes(StandardCharsets.UTF_8)), component.getUuid());
        
        component = new NameComponent("Test");
        log.info(component.toString());
        assertEquals(UUID.nameUUIDFromBytes("Test".getBytes(StandardCharsets.UTF_8)), component.getUuid());
        assertEquals(0, component.getAbbreviationLength());
        assertEquals("Test", component.getValue());
    
        component = new NameComponent("Testing", 4);
        log.info(component.toString());
        assertEquals(UUID.nameUUIDFromBytes("Test.".getBytes(StandardCharsets.UTF_8)), component.getUuid());
        assertEquals(4, component.getAbbreviationLength());
        assertEquals("Test.", component.getValue());
    }
    @Test
    void setValue() {
        String testValue = "test_value";
        UUID expectedUuid = UUID.nameUUIDFromBytes(testValue.getBytes(StandardCharsets.UTF_8));
        UUID emtpyStringUuid = UUID.nameUUIDFromBytes("".getBytes(StandardCharsets.UTF_8));
        
        NameComponent component = new NameComponent();
        log.info(component.toString());
    
        assertEquals(emtpyStringUuid, component.getUuid());
        component.setValue(null);
        assertEquals(emtpyStringUuid, component.getUuid());
        
        component.setValue(testValue);
        assertEquals(testValue, component.getValue());
        assertEquals(0, expectedUuid.compareTo(component.getUuid()));
        assertEquals(0, component.getAbbreviationLength());
        log.info(component.toString());
        
        component.setValue("nonsenseTestValue");
        assertNotEquals(expectedUuid, component.getUuid());
        log.info(component.toString());
    }
    
    @Test
    void abbreviationTest() {
        NameComponent component = new NameComponent();
        log.info(component.toString());
        
        component.setValue("hello");
        log.info(component.toString());
        assertEquals("hello", component.getValue());
        
        component.setAbbreviationLength(1);
        log.info(component.toString());
        assertEquals("h.", component.getValue());
        
        component.setAbbreviationLength(10);
        log.info(component.toString());
        assertEquals("hello", component.getValue());
    }
}
