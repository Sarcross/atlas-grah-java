package dev.haddox.atlasgrah;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class AtlasGrahApplicationTests {
    private final InputStream systemIn = System.in;
    
    private void provideInput(String data) {
        ByteArrayInputStream testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }
    
    @AfterEach
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
    }
    @Test
    void contextLoads() {
        assertDoesNotThrow(() -> AtlasGrahApplication.main(new String[] {"--server"}));
    }
    
    @Test
    void testConsoleApplication() {
        provideInput("\ngenerate\nhelp\nbad\nexit");
        assertDoesNotThrow(() -> AtlasGrahApplication.main(new String[] {}));
    }
}
