package dev.haddox.atlasgrah.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("dev.haddox.atlasgrah")
public class AppConfig {
}
