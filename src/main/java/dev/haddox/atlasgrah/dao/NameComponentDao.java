package dev.haddox.atlasgrah.dao;

import dev.haddox.atlasgrah.data.NameComponent;
import dev.haddox.atlasgrah.data.PartType;

import java.util.List;

public interface NameComponentDao {
    List<NameComponent> getComponents(PartType partType);
}
