package dev.haddox.atlasgrah.dao.impl;

import dev.haddox.atlasgrah.dao.NameComponentDao;
import dev.haddox.atlasgrah.data.NameComponent;
import dev.haddox.atlasgrah.data.PartType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
@Qualifier("NameComponentYamlDao")
@Slf4j
public class NameComponentYamlDao implements NameComponentDao {
    @SneakyThrows
    @Override
    public List<NameComponent> getComponents(PartType partType) {
        List<NameComponent> components = new ArrayList<>();
        InputStream resource = null;

        switch(partType) {
            case PREFIX:
                resource = new ClassPathResource("data/prefix.txt").getInputStream();
                break;
            case PRIMARY:
                resource = new ClassPathResource("data/primary.txt").getInputStream();
                break;
            case SUFFIX:
                resource = new ClassPathResource("data/suffix.txt").getInputStream();
                break;
        }

        assert resource != null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
            reader.lines().forEach(l -> components.add(new NameComponent(l)));
        }

        log.info("Added {} entries for {}", components.size(), partType);
        return components;
    }
}
