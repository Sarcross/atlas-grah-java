package dev.haddox.atlasgrah.console;

import dev.haddox.atlasgrah.dao.impl.NameComponentYamlDao;
import dev.haddox.atlasgrah.generator.BasicPlanetNameGenerator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;

@Slf4j
public class ConsoleApplication {
    private static final String[] commands = { "generate", "help", "exit" };
    private static BasicPlanetNameGenerator generator;

    private ConsoleApplication() {

    }

    public static void run() {
        initialize();
        printHelp();
        listenForInput();
    }

    private static void initialize() {
        log.info("Initializing application...");
        generator = new BasicPlanetNameGenerator();
        generator.setSeed(System.currentTimeMillis());
        generator.setDao(new NameComponentYamlDao());
        generator.populateParts();
        log.info("Initialization complete.");
    }

    private static void printHelp() {
        log.info("Commands: {}", (Object) commands);
        log.info("Type in a a command and press enter. To quick generate a name, simply press enter.");
    }

    @SneakyThrows
    private static void listenForInput() {
        boolean collectInput = true;
        String input;
        Scanner in = new Scanner(System.in);
        while(collectInput) {
            input = in.nextLine().trim();
            if("exit".equalsIgnoreCase(input)) {
                in.close();
                collectInput = false;
                log.info("Shutting down...");
            } else if("generate".equalsIgnoreCase(input) || input.length() == 0) {
                log.info("Generated: {}", generator.generate().toString());
            } else if("help".equalsIgnoreCase(input)) {
                printHelp();
            } else {
                log.info("Invalid selection.");
                printHelp();
            }
        }
    }
}
