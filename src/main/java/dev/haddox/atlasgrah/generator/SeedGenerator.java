package dev.haddox.atlasgrah.generator;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * Utility class to generate seeds for use by {@code Random}.
 *
 * @see java.util.Random
 */
@Slf4j
public final class SeedGenerator {
    private SeedGenerator() {

    }

    /**
     * Generates a seed value from a {@code String}, typically a system name,
     * and a timestamp. If a null {@code systemName} is provided, the seed
     * defaults to -1.
     *
     * @param systemName {@code String} containing the name of a system
     * @param timestamp  numeric timestamp from the system clock
     *
     * @return a numeric seed value
     */
    public static long generate(String systemName, long timestamp) {
        try {
            log.trace("Generating seed from: {} at {}", systemName, timestamp);
            var characterStreamArray = systemName.chars().toArray();
            log.trace("systemName length: {} | systemName Character Stream Array: {}", systemName.length(),
                      characterStreamArray);
            long seed = Arrays.stream(characterStreamArray).asLongStream().sum() + systemName.length() + timestamp;
            log.info("Generated seed: {}", seed);
            return seed;
        } catch(NullPointerException e) {
            log.error("Null system name. Returning -1");
            return - 1;
        }
    }
}
