package dev.haddox.atlasgrah.generator;

import dev.haddox.atlasgrah.dao.NameComponentDao;
import dev.haddox.atlasgrah.data.NameComponent;
import dev.haddox.atlasgrah.data.PartType;
import dev.haddox.atlasgrah.data.PlanetName;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Procedurally generates {@code PlanetName} objects.
 *
 * @see PlanetName
 */
@Slf4j
public class BasicPlanetNameGenerator {
    private static final NameComponent SPACE_COMPONENT = new NameComponent(" ");
    private static final NameComponent DASH_COMPONENT = new NameComponent("-");
    private static final NameComponent SLASH_COMPONENT = new NameComponent("/");
    private static final NameComponent DOUBLE_SLASH_COMPONENT = new NameComponent("//");
    private double prefixChance = 0.1f;
    private double suffixChance = 0.05f;
    private double joinerChance = 0.05f;
    private NameComponentDao nameComponentDao;
    @Getter
    @Setter
    private long generationCount;
    private Random random;
    @Getter
    private List<PlanetName> history;
    private List<NameComponent> prefixParts;
    private List<NameComponent> primaryParts;
    private List<NameComponent> suffixParts;
    private List<NameComponent> joinerParts;

    public BasicPlanetNameGenerator() {
        history = new ArrayList<>();
        prefixParts = new ArrayList<>();
        primaryParts = new ArrayList<>();
        suffixParts = new ArrayList<>();
        joinerParts = new ArrayList<>();

        log.info("BasicPlanetNameGenerator initialization finished");
    }

    /**
     * Sets the seed used by the random generator.
     *
     * @param seed seed value
     *
     * @see java.util.Random
     * @see dev.haddox.atlasgrah.generator.SeedGenerator
     */
    public void setSeed(long seed) {
        generationCount = 0;
        random = new Random(seed);
    }

    /**
     * Populates the DAO object that will select {@code NameComponents} used
     * during generation.
     *
     * @param dao preconfigured DAO object
     *
     * @see NameComponentDao
     */
    public void setDao(NameComponentDao dao) {
        this.nameComponentDao = dao;
    }

    /**
     * Populates the prefix, primary, and suffix parts Lists that are required
     * for generation. This method should ony be called after {@code setDao()}.
     */
    public void populateParts() {
        prefixParts = nameComponentDao.getComponents(PartType.PREFIX);
        primaryParts = nameComponentDao.getComponents(PartType.PRIMARY);
        suffixParts = nameComponentDao.getComponents(PartType.SUFFIX);

        joinerParts.add(SPACE_COMPONENT);
        joinerParts.add(DASH_COMPONENT);
        joinerParts.add(SLASH_COMPONENT);
        joinerParts.add(DOUBLE_SLASH_COMPONENT);
    }

    /**
     * Randomly selects and adds a {@code NameComponent} to a
     * {@code PlanetName} and increments the internal generation counter.
     *
     * @param name  the {@code PlanetName} to build on
     * @param parts List containing {@code NameComponents} to randomly select
     *              from
     *
     * @see NameComponent
     * @see PlanetName
     */
    private void addPart(PlanetName name, List<NameComponent> parts) {
        int intValue = random.nextInt(parts.size());
        generationCount++;
        name.addPart(parts.get(intValue));
    }

    /**
     * Tries to randomly select and add a {@code NameComponent} to a
     * {@code PlanetName} based upon a random {@code chance}.
     *
     * @param name   the {@code PlanetName} to build on
     * @param parts  List containing {@code NameComponents} to randomly select
     *               from
     * @param chance decimal-based percentage to add the part
     *
     * @return boolean indicating success or failure
     *
     * @see NameComponent
     * @see PlanetName
     */
    private boolean tryAddPart(PlanetName name, List<NameComponent> parts, double chance) {
        double gausianValue = random.nextGaussian();
        generationCount++;

        if(chance <= gausianValue) {
            int intValue = random.nextInt(parts.size());
            generationCount++;
            name.addPart(parts.get(intValue));

            return true;
        }

        return false;
    }

    /**
     * Tries to randomly select a joiner {@code NameComponent} to a random
     * {@code chance}.
     *
     * @param chance decimal-based percentage to select a non-standard joiner
     *
     * @return a joiner {@code NameComponent}
     *
     * @see NameComponent
     */
    private NameComponent tryGetJoiner(double chance) {
        NameComponent joiner = SPACE_COMPONENT;
        if(chance > 0.0) {
            double gausianValue = random.nextGaussian();
            generationCount++;

            if(chance <= gausianValue) {
                int intValue = random.nextInt(joinerParts.size());
                generationCount++;
                joiner = joinerParts.get(intValue);
            }
        }

        return joiner;
    }

    /**
     * Procedurally generates a {@code PlanetName} based upon the
     * {@code BasicPlanetNameGenerator}'s configuration.
     *
     * @return a procedurally generated {@code PlanetName}
     *
     * @see PlanetName
     */
    public PlanetName generate() {
        PlanetName name = new PlanetName();

        if(tryAddPart(name, prefixParts, prefixChance)) {
            name.addPart(tryGetJoiner(- 1));
        }
        addPart(name, primaryParts);
        if(tryAddPart(name, suffixParts, suffixChance)) {
            name.getParts().add(name.getParts().size() - 1, tryGetJoiner(joinerChance));
            name.calculateUuid();
        }
        history.add(name);

        return name;
    }
}
