package dev.haddox.atlasgrah.generator;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Utility class to generate {@code UUID}s.
 *
 * @see java.util.UUID
 */
public class UuidGenerator {
    private UuidGenerator() {

    }

    /**
     * Generates a {@code UUID} from a {@code String}.
     *
     * @param value {@code String} to create a {@code UUID} from
     *
     * @return a {@code UUID} object
     *
     * @see java.util.UUID
     */
    public static UUID fromString(String value) {
        return UUID.nameUUIDFromBytes(value.getBytes(StandardCharsets.UTF_8));
    }
}
