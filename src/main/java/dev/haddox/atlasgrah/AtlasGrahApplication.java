package dev.haddox.atlasgrah;

import dev.haddox.atlasgrah.console.ConsoleApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Objects;

@EnableAsync
@SpringBootApplication
public class AtlasGrahApplication {
    public static void main(String[] args) {
        if(args.length != 0 && (Objects.equals(args[0], "--server") || Objects.equals(args[0], "-s"))) {
            SpringApplication.run(AtlasGrahApplication.class, args);
        } else {
            ConsoleApplication.run();
        }
    }
}
