package dev.haddox.atlasgrah.data;

public enum PartType {
    PREFIX,
    PRIMARY,
    SECONDARY,
    TERTIARY,
    SUFFIX
}
