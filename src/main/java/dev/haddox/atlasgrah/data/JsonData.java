package dev.haddox.atlasgrah.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.SneakyThrows;

public abstract class JsonData {
    ObjectMapper mapper;

    @SneakyThrows
    public String toString() {
        if(mapper == null) {
            mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        }

        return mapper.writeValueAsString(this);
    }
}
