package dev.haddox.atlasgrah.data;

import dev.haddox.atlasgrah.generator.UuidGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PlanetName {
    UUID uuid;

    List<NameComponent> parts;

    public PlanetName() {
        uuid = null;
        parts = new ArrayList<>();
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public List<NameComponent> getParts() {
        return this.parts;
    }

    public String toString() {
        return parts.stream().map(NameComponent::getValue).collect(Collectors.joining());
    }

    public void addPart(NameComponent part) {
        parts.add(part);
        calculateUuid();
    }

    public void calculateUuid() {
        uuid = UuidGenerator.fromString(this.toString());
    }
}
