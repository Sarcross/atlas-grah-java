package dev.haddox.atlasgrah.data;

import dev.haddox.atlasgrah.generator.UuidGenerator;
import lombok.Getter;

import java.util.UUID;

public class NameComponent extends JsonData {
    @Getter
    private UUID uuid;

    private String value;

    @Getter
    private int abbreviationLength;

    public NameComponent() {
        setValue("");
    }

    public NameComponent(String value) {
        setValue(value);
    }

    public NameComponent(String value, int abbreviationLength) {
        setValue(value, abbreviationLength);
    }

    public void setValue(String value, int abbreviationLength) {
        this.value = value;
        this.abbreviationLength = abbreviationLength;
        this.uuid = UuidGenerator.fromString(getValue());
    }

    public String getValue() {
        if(abbreviationLength == 0) {
            return value;
        }
        if(abbreviationLength > value.length()) {
            return value;
        }

        return value.substring(0, abbreviationLength) + ".";
    }

    public void setValue(String value) {
        if(value == null) {
            value = "";
        }
        setValue(value, 0);
    }

    public void setAbbreviationLength(int abbreviationLength) {
        this.abbreviationLength = abbreviationLength;
        this.uuid = UuidGenerator.fromString(getValue());
    }
}
